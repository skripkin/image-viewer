# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is simple image viewer project for test assignment
* Version: 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up:
Java: jdk version 1.7 or higher 
Application Server: glassfish 4.1
Data base: MySql server (latest version), use viewer_dump.sql to create schema and tables

* Deployment instructions:
Run package command from root pom.xml file (use mvn -package).
As a result you will get viewer.ear package for deployment to application server (viewer_ear\target\viewer.ear)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact