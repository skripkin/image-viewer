package com.viewer.common.ejb;

import com.viewer.common.persistence.User;
import com.viewer.common.persistence.UserPassword;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class UserBean extends BaseBean implements UserBeanLocal {

    public List<User> getAllUsers() throws PersistenceException {
        return null;
    }

    public User getUserByEmail(String email) throws PersistenceException {
        User user = null;

        if (email == null || email.isEmpty()) {
            return user;
        }

        String sqlStr = "Select * From t_user Where user_email = '" + email +"'";
        Query sql = getEntityManager().createNativeQuery(sqlStr, User.class);

        List<User> users = sql.getResultList();
        if (users == null || users.size() == 0) {
            return user;
        }
        user = users.get(0);

        return user;
    }

    public User save(User user) throws PersistenceException {
        if (user.getId() <= 0) {
            //create new user
            persist(user);
            flush();

            //store password
            UserPassword userPassword = new UserPassword();
            String encryptedPassword = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(11));
            userPassword.setPassword(encryptedPassword);
            userPassword.setUserId(user.getId());
            persist(userPassword);
        } else {
            //update user todo
        }
        return user;
    }

    public User validatePassword(String email, String password) throws PersistenceException {
        User user = null;

        if (email == null || email.isEmpty()) {
            return user;
        }

        if (password == null || password.isEmpty()) {
            return user;
        }

        //validate email
        user = getUserByEmail(email);
        if (user == null) {
            return user;
        }


        //validate password
        String sqlStr = "Select * From t_user_pass Where pass_user_id = " + user.getId();
        Query sql = getEntityManager().createNativeQuery(sqlStr, UserPassword.class);

        List<UserPassword> passwords = sql.getResultList();
        if (passwords == null || passwords.size() == 0) {
            //no password found for specified user (should not happened)
            user = null;
            return user;
        }
        UserPassword userPassword = passwords.get(0);
        boolean isValid = BCrypt.checkpw(password, userPassword.getPassword());

        user = isValid ? user : null;

        return user;
    }
}
