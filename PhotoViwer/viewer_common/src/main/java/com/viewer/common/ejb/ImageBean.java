package com.viewer.common.ejb;

import com.viewer.common.persistence.Image;

import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class ImageBean extends BaseBean implements ImageBeanLocal {
    private static List<Image> images;

    public List<Image> getAllImages() throws PersistenceException {
        List<Image> images;

        String sqlStr = "Select * From t_image";
        Query sql = getEntityManager().createNativeQuery(sqlStr, Image.class);

        images = sql.getResultList();

        return images;
    }

    public Image save(Image image) throws PersistenceException {
        if (image.getId() <= 0) {
            persist(image);
        }

        return image;
    }
}
