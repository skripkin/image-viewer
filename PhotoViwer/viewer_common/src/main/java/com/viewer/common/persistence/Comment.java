package com.viewer.common.persistence;


import javax.persistence.*;
import java.io.Serializable;

@SqlResultSetMapping(name = "AllComments",
        entities = {@EntityResult(entityClass = Comment.class)},
        columns = {@ColumnResult(name = "comment_owner")}
)

@Entity
@Table(name = "t_comment")
public class Comment implements Serializable {
    private long id;
    private String commentText;
    private long imageId;
    private long userId;
    private String commentOwner;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "comment_txt")
    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    @Column(name = "comment_img")
    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }

    @Column(name = "comment_user")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Transient
    public String getCommentOwner() {
        return commentOwner;
    }

    public void setCommentOwner(String commentOwner) {
        this.commentOwner = commentOwner;
    }
}
