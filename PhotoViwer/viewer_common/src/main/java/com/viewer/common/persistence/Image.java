package com.viewer.common.persistence;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_image")
public class Image implements Serializable {
    private long id;
    private String name;
    private String description;
    private byte[] imageSource;
    private long userId;
    private long albumId;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "img_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "img_dsc")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Lob
    @Column(name = "img_data")
    public byte[] getImageSource() {
        return imageSource;
    }

    public void setImageSource(byte[] imageSource) {
        this.imageSource = imageSource;
    }

    @Column(name = "img_user")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Column(name = "img_album")
    public long getAlbumId() {
        return albumId;
    }

    public void setAlbumId(long albumId) {
        this.albumId = albumId;
    }
}
