package com.viewer.common.ejb;

import com.viewer.common.persistence.Comment;

import javax.ejb.Local;
import javax.persistence.PersistenceException;
import java.util.List;

@Local
public interface CommentBeanLocal {

    List<Comment> getCommentsByImg(long imageId) throws PersistenceException;

    Comment save(Comment comment) throws PersistenceException;
}
