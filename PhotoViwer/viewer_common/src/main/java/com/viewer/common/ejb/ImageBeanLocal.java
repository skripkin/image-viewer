package com.viewer.common.ejb;

import com.viewer.common.persistence.Image;

import javax.ejb.Local;
import javax.persistence.PersistenceException;
import java.util.List;

@Local
public interface ImageBeanLocal {

    List<Image> getAllImages() throws PersistenceException;

    Image save(Image image) throws PersistenceException;
}
