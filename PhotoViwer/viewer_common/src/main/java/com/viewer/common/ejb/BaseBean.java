package com.viewer.common.ejb;

import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

@Stateless
public class BaseBean {
    @PersistenceContext(unitName = "viewerPU")
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public <E> void persist(E entity) throws PersistenceException {
        try {
            entityManager.persist(entity);
        } catch (Throwable ex) {
            throw new PersistenceException("Error persisting entity", ex);
        }
    }

    public <E> void remove(E entity) throws PersistenceException {
        try {
            entityManager.remove(entity);
        } catch (Throwable ex) {
            throw new PersistenceException("Error removing entity", ex);
        }
    }

    public <E> E getEntity(Class<E> entityClass, Long id) {
        return entityManager.find(entityClass, id);
    }

    public <E> void merge(E entity) {
        entityManager.merge(entity);
    }

    public void flush() {
        entityManager.flush();
    }
}
