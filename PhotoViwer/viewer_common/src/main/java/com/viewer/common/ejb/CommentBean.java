package com.viewer.common.ejb;

import com.viewer.common.persistence.Comment;

import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class CommentBean extends BaseBean implements CommentBeanLocal {

    public List<Comment> getCommentsByImg(long imageId) throws PersistenceException {
        List<Comment> result = new ArrayList<>();

        String sqlStr = "Select c.*, u.user_email as comment_owner " +
                "From t_comment c " +
                "Join t_user u on u.id = c.comment_user " +
                "Where c.comment_img = " + imageId;
        Query sql = getEntityManager().createNativeQuery(sqlStr, "AllComments");

        List<Object[]> resultList = sql.getResultList();
        for (Object[] row : resultList) {
            Comment comment = (Comment) row[0];
            comment.setCommentOwner((String) row[1]);

            result.add(comment);
        }

        return result;
    }

    public Comment save(Comment comment) throws PersistenceException {
        if (comment.getId() <= 0) {
            persist(comment);
        } else {
            merge(comment);
        }

        return comment;
    }
}
