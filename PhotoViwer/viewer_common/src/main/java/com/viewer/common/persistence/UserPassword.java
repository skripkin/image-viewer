package com.viewer.common.persistence;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_user_pass")
public class UserPassword implements Serializable {
    private long id;
    private long userId;
    private String password;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "pass_user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Column(name = "pass_pwd")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
