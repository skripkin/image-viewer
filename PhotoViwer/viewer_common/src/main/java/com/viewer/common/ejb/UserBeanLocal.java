package com.viewer.common.ejb;

import com.viewer.common.persistence.User;

import javax.ejb.Local;
import javax.persistence.PersistenceException;
import java.util.List;

@Local
public interface UserBeanLocal {

    List<User> getAllUsers() throws PersistenceException;

    User save(User user) throws PersistenceException;

    User validatePassword(String email, String password) throws PersistenceException;
}
