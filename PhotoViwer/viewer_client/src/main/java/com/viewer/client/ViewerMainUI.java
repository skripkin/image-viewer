package com.viewer.client;


import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;
import com.viewer.client.ui.view.MainViewer;

import javax.servlet.annotation.WebServlet;

@Theme("viewer")
@Widgetset("com.viewer.client.ViewerAppWidgetset")
public class ViewerMainUI extends UI {
    private Navigator navigator;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        // Create new instance of the navigator. It will attach itself automatically to this view.
        navigator = new Navigator(this, this);
        this.setNavigator(navigator);

        navigator.addView(MainViewer.NAME, MainViewer.class);

        navigator.navigateTo(MainViewer.NAME);
    }

    @WebServlet(urlPatterns = "/*", name = "ViewerUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = ViewerMainUI.class, productionMode = false)
    public static class ViewerUIServlet extends VaadinServlet {
    }
}
