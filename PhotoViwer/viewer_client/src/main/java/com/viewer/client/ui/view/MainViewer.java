package com.viewer.client.ui.view;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.event.MouseEvents;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import com.vaadin.ui.declarative.Design;
import com.viewer.client.business.EjbServiceLocator;
import com.viewer.client.ui.window.LoginWindow;
import com.viewer.client.ui.window.ShowImageWindow;
import com.viewer.client.ui.window.SignUpWindow;
import com.viewer.client.ui.window.UploadImageWindow;
import com.viewer.common.ejb.ImageBeanLocal;
import com.viewer.common.ejb.UserBeanLocal;
import com.viewer.common.persistence.Image;
import com.viewer.common.persistence.User;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

@DesignRoot
public class MainViewer extends VerticalLayout implements View {
    public static final String NAME = "main";

    private VerticalLayout photoLayout;
    private GridLayout imageLayout;
    private Button loginBtn;
    private Button signBtn;
    private Button uploadBtn;
    private Button logoutBtn;

    private User currentUser;

    private UserBeanLocal userBean;
    private ImageBeanLocal imageBean;


    public MainViewer() {
        Design.read(this);

        //init ejb
        userBean = (UserBeanLocal) EjbServiceLocator.getInstance().lookupEjb("UserBean");
        imageBean = (ImageBeanLocal) EjbServiceLocator.getInstance().lookupEjb("ImageBean");

        currentUser = null;
    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        init();
    }

    private void init() {
        //login click listener
        loginBtn.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                login();
            }
        });

        //sign up click listener
        signBtn.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                signUp();
            }
        });

        //logout click listener
        logoutBtn.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                VaadinSession.getCurrent().setAttribute("user", null);
                VaadinSession.getCurrent().close();
                getUI().getPage().setLocation("/viewer");
                currentUser = null;
            }
        });

        //upload click listener
        uploadBtn.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                uploadImage();
            }
        });

        imageLayout.setSizeUndefined();
        //load images
        refreshImageLayout();
    }

    private void login() {
        Window window = new LoginWindow();
        window.addCloseListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent closeEvent) {
                currentUser = (User) VaadinSession.getCurrent().getAttribute("user");
                handleHeader(currentUser != null);
            }
        });
    }

    private void signUp() {
        Window window = new SignUpWindow();
        window.addCloseListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent closeEvent) {
                currentUser = (User) VaadinSession.getCurrent().getAttribute("user");
                handleHeader(currentUser != null);
            }
        });
    }

    private void uploadImage() {
        Window window = new UploadImageWindow();
        window.addCloseListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent closeEvent) {
                refreshImageLayout();
            }
        });

    }

    private void openImage(Embedded embedded, Image image) {
        if (embedded == null || image == null) {
            return;
        }
        new ShowImageWindow(embedded, image);

    }

    private void refreshImageLayout() {
        imageLayout.removeAllComponents();
        int columns = (getUI().getPage().getBrowserWindowWidth() - 100) / 200;
        imageLayout.setColumns(columns);

        List<Image> result = imageBean.getAllImages();
        int rows = result.size() / columns;
        rows = rows > 0 ? rows : 2;
        imageLayout.setRows(rows);

        for (Image image : result) {
            imageLayout.addComponent(getEmbeddedComponent(image));
        }
    }

    private Embedded getEmbeddedComponent(final Image image) {
        Embedded result = new Embedded();
        result.setWidth("200px");
        result.setHeight("230px");
        result.addClickListener(new MouseEvents.ClickListener() {
            @Override
            public void click(MouseEvents.ClickEvent clickEvent) {
                Embedded img = (Embedded) clickEvent.getSource();
                if (img != null) {
                    openImage(img, (Image)img.getData());
                }
            }
        });

        Resource resource;
        StreamResource.StreamSource streamResource = new StreamResource.StreamSource() {
            @Override
            public InputStream getStream() {
                byte imageByte[] = image.getImageSource();
                return imageByte == null ? null : new ByteArrayInputStream(imageByte);
            }
        };
        resource = new StreamResource(streamResource, image.getName());
        result.setSource(resource);
        result.setData(image);

        return result;
    }

    private void handleHeader(boolean login) {
        loginBtn.setVisible(!login);
        signBtn.setVisible(!login);

        uploadBtn.setVisible(login);
        logoutBtn.setVisible(login);
    }
}
