package com.viewer.client.ui.window;


import com.vaadin.annotations.DesignRoot;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.declarative.Design;
import com.viewer.client.business.EjbServiceLocator;
import com.viewer.common.ejb.UserBeanLocal;
import com.viewer.common.persistence.User;

@DesignRoot
public class SignUpWindow extends ParentWindow {
    private TextField firstName;
    private TextField lastName;
    private TextField email;
    private PasswordField password;
    private PasswordField confirmPassword;
    private Button signUp;

    private UserBeanLocal userBean;

    public SignUpWindow() {
        Design.read(this);

        //init ejb
        userBean = (UserBeanLocal) EjbServiceLocator.getInstance().lookupEjb("UserBean");

        init();

        this.center();
        getUI().getCurrent().addWindow(this);
    }

    private void init() {
        //signup lick listener
        signUp.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                createUser();
            }
        });

    }

    private void createUser() {
        //check first name
        String firstNme = firstName.getValue();
        if (firstNme == null || firstNme.isEmpty()) {
            showNotification("warning", "First name is missing.");
            return;
        }

        //check last name
        String lastNme = lastName.getValue();
        if (lastNme == null || lastNme.isEmpty()) {
            showNotification("warning", "Last name is missing.");
            return;
        }

        //check email
        String eml = email.getValue();
        if (eml == null || eml.isEmpty()) {
            showNotification("warning", "Email is missing.");
            return;
        }

        //check password
        String pwd = password.getValue();
        if (pwd == null || pwd.isEmpty()) {
            showNotification("warning", "Password is missing.");
            return;
        }
        String confirmPwd = confirmPassword.getValue();
        if (!pwd.equals(confirmPwd)) {
            showNotification("warning", "User password mismatch.");
            return;
        }

        User newUser = new User();
        newUser.setFirstName(firstNme);
        newUser.setLastName(lastNme);
        newUser.setEmail(eml);
        newUser.setPassword(pwd);

        try {
            newUser = userBean.save(newUser);
            VaadinSession.getCurrent().setAttribute("user", newUser);
        } catch(Exception ex) {
            showNotification("error", "Error while saving new user: " + ex.getMessage());
        }

        this.close();
    }
}
