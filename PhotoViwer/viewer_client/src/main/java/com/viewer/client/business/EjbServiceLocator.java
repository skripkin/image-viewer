package com.viewer.client.business;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.HashMap;
import java.util.Map;

public class EjbServiceLocator {
    private Context initialContext;
    private Map cache;

    private static String EJB_LOOKUP_PREFIX = "java:global/viewer/viewer_common-0.1/";
    private static EjbServiceLocator instance = new EjbServiceLocator();

    public static EjbServiceLocator getInstance() {
        return instance;
    }

    private EjbServiceLocator() {
        cache = new HashMap();
        try {
            this.initialContext = new InitialContext();
        } catch(NamingException ex) {
            System.out.printf(
                    "Error in CTX looking up %s because of %s while %s",
                    ex.getRemainingName(),
                    ex.getCause(),
                    ex.getExplanation());
        }
    }

    public Object lookupEjb(String ejbName) {
        if(this.cache.containsKey(ejbName)) {
            return this.cache.get(ejbName);
        } else {
            try {
                Object ejbRef = initialContext.lookup(EJB_LOOKUP_PREFIX + ejbName);
                this.cache.put(ejbName, ejbRef);
                return  ejbRef;
            } catch(NamingException ex) {
                throw new RuntimeException(ex);
            } catch(Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
