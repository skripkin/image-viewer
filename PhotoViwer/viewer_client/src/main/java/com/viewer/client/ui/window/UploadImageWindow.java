package com.viewer.client.ui.window;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Upload;
import com.vaadin.ui.declarative.Design;
import com.viewer.client.business.EjbServiceLocator;
import com.viewer.common.ejb.ImageBeanLocal;
import com.viewer.common.persistence.Image;
import com.viewer.common.persistence.User;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@DesignRoot
public class UploadImageWindow extends ParentWindow {
    private Upload loadImage;
    private Embedded uploadImage;
    private Button saveImage;

    private Image currentImage;
    private User currentUser;

    private ImageBeanLocal imageBean;


    public UploadImageWindow() {
        Design.read(this);

        //init ejb
        imageBean = (ImageBeanLocal) EjbServiceLocator.getInstance().lookupEjb("ImageBean");

        init();

        this.center();
        getUI().getCurrent().addWindow(this);
    }

    private void init() {
        currentUser = (User) VaadinSession.getCurrent().getAttribute("user");
        System.out.println("CurrentUser id: " + currentUser.getId());
        currentImage = null;

        //init image upload
        ImageReceiver receiver = new ImageReceiver();
        loadImage.setImmediate(true);
        loadImage.setReceiver(receiver);
        loadImage.addSucceededListener(receiver);

        //add click listener to save button
        saveImage.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                save();
            }
        });
    }

    private void save() {
        if (currentImage != null) {
            imageBean.save(currentImage);
            this.close();
        }
    }

    private void createImage(byte[] image, String fileName) {
        if (currentImage == null) {
            //create new image
            currentImage = new Image();
            currentImage.setName(fileName);
            currentImage.setUserId(currentUser.getId());
        }
        currentImage.setImageSource(image);
    }

    // Implement both receiver that saves upload in a file and
    // listener for successful upload
    class ImageReceiver implements Upload.Receiver, Upload.SucceededListener {
        public File file;

        public OutputStream receiveUpload(String filename,
                                          String mimeType) {
            // Create upload stream
            FileOutputStream fos = null; // Stream to write to
            try {
                // Open the file for writing.
                file = new File("C:/temp/resource/" + filename);
                fos = new FileOutputStream(file);
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Could not open file<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            }
            return fos; // Return the output stream to write to
        }

        public void uploadSucceeded(Upload.SucceededEvent event) {
            // Show the uploaded file in the image viewer
            uploadImage.setVisible(true);
            uploadImage.setSource(new FileResource(file));

            Path path = Paths.get(file.getAbsolutePath());
            byte[] image = null;
            try {
                image = Files.readAllBytes(path);
            } catch(Exception ex) {
                showNotification("error", "Error while setting new avatar");
            }

            if (image != null && image.length > 0) {
                createImage(image, file.getName());
            }
        }
    }
}
