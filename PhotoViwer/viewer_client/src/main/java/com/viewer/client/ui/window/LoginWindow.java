package com.viewer.client.ui.window;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.declarative.Design;
import com.viewer.client.business.EjbServiceLocator;
import com.viewer.common.ejb.UserBeanLocal;
import com.viewer.common.persistence.User;


@DesignRoot
public class LoginWindow extends ParentWindow {
    private TextField email;
    private PasswordField password;
    private Button signIn;

    private FieldGroup form;

    private UserBeanLocal userBean;


    public LoginWindow() {
        Design.read(this);

        //init ejb
        userBean = (UserBeanLocal) EjbServiceLocator.getInstance().lookupEjb("UserBean");

        init();

        this.center();
        getUI().getCurrent().addWindow(this);
    }

    private void init() {
        User user = new User();
        user.setEmail("");
        user.setPassword("");

        email.setImmediate(true);
        email.setBuffered(true);

        BeanItem<User> userBeanItem = new BeanItem<User>(user);
        form = new FieldGroup(userBeanItem);

        form.bind(email, "email");
        form.bind(password, "password");

        signIn.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                login();
            }
        });
    }

    private void login() {
        try {
            form.commit();
        } catch(FieldGroup.CommitException ex) {

        }

        BeanItem<User> formItem = (BeanItem<User>)form.getItemDataSource();
        User loginUser = formItem.getBean();

        User user = userBean.validatePassword(loginUser.getEmail(), loginUser.getPassword());

        if (user != null) {
            VaadinSession.getCurrent().setAttribute("user", user);
            this.close();
        } else {
            //todo else notification about missing/invalid password
            showNotification("warning", "User or password is invalid.");
        }
    }
}
