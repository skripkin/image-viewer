package com.viewer.client.ui.window;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import com.vaadin.ui.Image;
import com.vaadin.ui.declarative.Design;
import com.viewer.client.business.EjbServiceLocator;
import com.viewer.common.ejb.CommentBeanLocal;
import com.viewer.common.persistence.*;

import java.util.ArrayList;
import java.util.List;

@DesignRoot
public class ShowImageWindow extends ParentWindow {
    private Embedded showImage;
    private TextArea newComment;
    private VerticalLayout imageCommentsLayout;
    private VerticalLayout newCommentLayout;
    private Button addCommentBtn;

    private User currentUser;
    private com.viewer.common.persistence.Image currentImage;
    private List<Comment> comments;

    private CommentBeanLocal commentBean;


    public ShowImageWindow(Embedded embedded, com.viewer.common.persistence.Image image) {
        Design.read(this);

        //init ejb
        commentBean = (CommentBeanLocal) EjbServiceLocator.getInstance().lookupEjb("CommentBean");

        this.showImage.setSource(embedded.getSource());
        this.currentImage = image;
        init();

        this.center();
        getUI().getCurrent().addWindow(this);
    }

    private void init() {
        comments = new ArrayList<>();

        currentUser = (User) VaadinSession.getCurrent().getAttribute("user");
        if (currentUser == null) {
            newCommentLayout.setVisible(false);
        } else {
            newCommentLayout.setVisible(true);
        }

        //add comment click listener
        addCommentBtn.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                addNewComment();
            }
        });

        imageCommentsLayout.setSizeUndefined();

        comments = commentBean.getCommentsByImg(currentImage.getId());
        if (comments != null && comments.size() > 0) {
            refreshComments();
        }

    }

    private void refreshComments() {
        if (comments == null || comments.size() == 0) {
            return;
        }
        imageCommentsLayout.removeAllComponents();

        for (Comment comment : comments) {
            String commentOwner = comment.getCommentOwner();
            String commentTxt = comment.getCommentText();
            commentTxt = commentTxt.replaceAll("\n", "<br/>");

            Label commentOwnerLabel = new Label();
            commentOwnerLabel.setContentMode(Label.CONTENT_XHTML);
            commentOwnerLabel.setValue("<b>" + commentOwner + "</b>");

            Label plainText = new Label();
            plainText.setContentMode(Label.CONTENT_XHTML);
            plainText.setValue(commentTxt);

            imageCommentsLayout.addComponent(commentOwnerLabel);
            imageCommentsLayout.addComponent(plainText);
        }

    }

    private void addNewComment() {
        Comment comment  = new Comment();

        comment.setCommentOwner(currentUser.getEmail());
        comment.setUserId(currentUser.getId());
        comment.setCommentText(newComment.getValue());
        comment.setImageId(currentImage.getId());

        try {
            commentBean.save(comment);
        } catch(Exception ex) {
            showNotification("error", "Error while saving new comments.");
        }
        comments.add(comment);
        refreshComments();

        newComment.setValue("");
    }
}
