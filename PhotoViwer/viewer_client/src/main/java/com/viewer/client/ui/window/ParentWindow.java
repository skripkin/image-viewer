package com.viewer.client.ui.window;


import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Window;

public class ParentWindow extends Window {

    protected void showNotification(String type, String messgae) {
        //init notification
        Notification notification = new Notification("");
        notification.setPosition(Position.MIDDLE_CENTER);
        notification.setDescription(messgae);
        notification.setStyleName(type);
        notification.show(Page.getCurrent());
    }
}
